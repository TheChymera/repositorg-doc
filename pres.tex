\documentclass[xcolor=table,aspectratio=169,dvipsnames]{beamer}
\usepackage{bm}
\usepackage[utf8]{inputenc}
\usepackage{graphviz}
\usepackage{amsmath}
\usepackage{empheq}
\usepackage{booktabs}
\usepackage{color}
\usepackage{minted}
\usepackage{multicol}
\usepackage{siunitx} %pretty measurement unit rendering
\usepackage[thinlines]{easytable}
\usepackage{hyperref} %enable hyperlink for urls
\usepackage{soul} %strikethrough via \st{}

\newcommand{\myhfill}{\hskip0pt plus 1filll}

\DeclareUnicodeCharacter{00A0}{ }
\setbeamersize{text margin left=0.8em,text margin right=0.8em}

\DeclareSIUnit\pixel{px}

\usecolortheme[RGB={199,199,199}]{structure}
\usetheme{Dresden}

\newenvironment{figurehere}
{\def\@captype{figure}}
{}
\makeatother

\definecolor{dy}{RGB}{202,202,0}
\definecolor{mg}{gray}{0.30}
\definecolor{lg}{gray}{0.60}
\definecolor{vlg}{gray}{0.75}
\definecolor{tlg}{gray}{0.92}
\definecolor{myred}{HTML}{EF0533}
\definecolor{mybrown}{HTML}{8B4513}

\definecolor{mypurple}{HTML}{8600E9}
\definecolor{mymagenta}{HTML}{D600D6}
\definecolor{mycyan}{HTML}{00A9A9}
\definecolor{mygreen}{HTML}{00Df1F}
\definecolor{myblue}{HTML}{0004F7}
\definecolor{myyellow}{HTML}{CAA200}
\definecolor{myred}{HTML}{E41F00}

\setbeamercolor{caption name}{fg=lg}
\setbeamercolor{caption}{fg=lg}
\setbeamercolor{author}{fg=lg}
\setbeamercolor{institute}{fg=lg}
\setbeamercolor{date}{fg=lg}
\setbeamercolor{title}{fg=mg}
\setbeamertemplate{caption}{\centering\insertcaption\par}
\setbeamertemplate{navigation symbols}{}

\title[Repositorg --- FOSS Workflows for Transferring, Repositing, Renaming, and Standardizing Large Data Collections]{Repostiorg}
\subtitle{Free and Open Source Workflows for\\ Transferring, Repositing, Renaming, and Standardizing Large Data Collections}
\author{Horea-Ioan Ioanas}
\institute{Institute for Biomedical Engineering, ETH and University of Zürich}
\begin{document}
	\begin{frame}
		\titlepage
		\centering\scriptsize [ \href{https://bitbucket.org/TheChymera/repositorg-doc}{\texttt{bitbucket.org/TheChymera/repositorg-doc}} ]\\
		\centering\scriptsize [ \href{https://github.com/TheChymera/repositorg}{\texttt{github.org/TheChymera/repositorg}} ]
	\end{frame}
	\section{Relevance}
		\subsection{Why care about repositing?}
			\begin{frame}{Big Data}
				\hspace*{\fill}= data too big for traditional processing/analysis to adequately deal with.\hspace*{\fill}
				
				\vspace{2em}
				Too big to:
				\begin{itemize}
					\item personally chaperone transfer.
					\item manually enforce record consistency.
					\item analyze without good knowledge of programming and statistics.
				\end{itemize}
				
				\vspace{1em}
				Big enough to:
				\begin{itemize}
					\item produce better statistical estimates.
					\item allow exploratory hypothesis testing.
				\end{itemize}
			\end{frame}
			\begin{frame}{Transparency and Collaboration}
				Big Data competency requirements are best met via collaboration:
				\begin{multicols}{3}
					\begin{itemize}
						\item with other researchers.
						\item with yourself across time.
						\item between tools.
					\end{itemize}
				\end{multicols}
				\vspace{-1.6em}
				\begin{figure}
					\centering
					\includedot[width=1\textwidth]{data/sharing}	
				\end{figure}
				\vspace{-1.6em}
				\begin{itemize}
					\item Data sharing need not become yet another competency requirement.
					\item But it is necessary to integrate competencies.
				\end{itemize}
			\end{frame}
			\begin{frame}{Open Formats/Structures}
				\begin{itemize}
					\item The earliest bottlenecks are the most consequential.
					\item The rawest data is the best possible recourse.
				\end{itemize}
				\begin{figure}
					\centering
					\includedot[width=1\textwidth]{data/sharinghighlight}	
				\end{figure}
			\end{frame}
	\section{Key Concepts}
		\subsection{Short and simple concepts guiding Repositorg.}
			\begin{frame}{Flexible Formats/Structures}
				\begin{itemize}
					\item Standards \textbf{can} work but \textbf{do} proliferate (e.g. BIDS\cite{Gorgolewski2016}, ISMRMRD\cite{Inati2016}).
					\item Flexible input/output is how to span the gamut (e.g. nibabel\cite{nibabel}).
				\end{itemize}
				\center
				\vspace{1em}
				Unstandardized:
			
				\texttt{\textcolor{mg}{<base>/MyUser/2018Jun1/\textcolor{myred}{5700}\_\textcolor{mygreen}{A3}\_\textcolor{myblue}{5}x\_w4\textcolor{myyellow}{transmission} new.\textcolor{mypurple}{TIF}}}
				
				\vspace{1em}
				Standardized:
			
				\texttt{\textcolor{mg}{<base>/\textcolor{myred}{sub-5700}/\textcolor{myred}{sub-5700}\_\textcolor{mygreen}{slice-a3}\_\textcolor{myblue}{zoom-5}\_\textcolor{myyellow}{transmission}.\textcolor{mypurple}{tif}}}
			\end{frame}
			\begin{frame}{Regex In, Formatable String Out}
				\center
				Regex\cite{regex} (parsimonious but all-powerful):
				\vspace{0.6em}
			
				\scriptsize
				\texttt{\textcolor{mg}{\^{}(?P<\textcolor{myred}{subject}>.+?)\_(?P<\textcolor{mygreen}{slice}>.+?)\_(?P<\textcolor{myblue}{zoom}>[0-9]+?)x\_w[1-9]*(?P<\textcolor{myyellow}{modality}>.+?)( .*)?\textbackslash.(?P<\textcolor{mypurple}{extension}>.+?)\$}}
				
				Matches: \texttt{\textcolor{mg}{\textcolor{myred}{5700}\_\textcolor{mygreen}{A3}\_\textcolor{myblue}{5}x\_w4\textcolor{myyellow}{transmission} new.\textcolor{mypurple}{TIF}}}
				
				\vspace{2em}
				\normalsize
				Extended Python string formatting (easy and straightforward):
				\vspace{0.6em}
			
				\footnotesize
				\texttt{\textcolor{mg}{sub-\{\textcolor{myred}{subject}\}/sub-\{\textcolor{myred}{subject}\}\_slice-\{\textcolor{mygreen}{slice!l}\}\_zoom-\{\textcolor{myblue}{zoom}\}\_\{\textcolor{myyellow}{modality!l}\}.\{\textcolor{mypurple}{extension!l}\}}}
				
				Generates: \texttt{\textcolor{mg}{sub-\textcolor{myred}{5700}/sub-\textcolor{myred}{5700}\_slice-\textcolor{mygreen}{a3}\_zoom-\textcolor{myblue}{5}\_\textcolor{myyellow}{transmission}.\textcolor{mypurple}{tif}}}
			
			\end{frame}
			\begin{frame}{Minimal and Deterministic Preprocessing}
				\begin{itemize}
					\item Only when needed for \textbf{raw} data standardization.
					\item Nondestructive.
					\item Deterministic and constant over time.
				\end{itemize}
				\vspace{-1em}
				\begin{figure}
					\includedot[width=1.03\textwidth]{data/workflow}	
				\end{figure}
			\end{frame}
			\begin{frame}{Compartmentalization}
				\begin{itemize}
					\item Flat is better than nested.
					\item Automatic is better than manual.
					\item Out of sight is better than in focus.
				\end{itemize}
				\vspace{-1em}
				\begin{figure}
					\includedot[width=1.03\textwidth]{data/compartmentalized}	
				\end{figure}
			\end{frame}
	\section{Data Sources}
		\subsection{Where is your data and how can you fetch it?}
			\begin{frame}{First Things First}
				\begin{figure}
					\centering
					\includedot[width=1.03\textwidth]{data/ftf}	
				\end{figure}
			\end{frame}
			\begin{frame}{Removable Media}
				\vspace{-2.3em}
				\begin{figure}
					\centering
					\includedot[width=0.66\textwidth]{data/rm}	
				\end{figure}
				\vspace{-2.7em}
				\begin{itemize}
					\item Client-side transfer.
					\item Best triggered via mount events, e.g. \colorbox{tlg}{\texttt{~/.repositorg/UUID\_trigger.sh}}:
				\end{itemize}
				\inputminted[bgcolor=tlg,firstline=1,lastline=4]{bash}{scripts/UUID_trigger.sh}
			\end{frame}
			\begin{frame}{Unix-like Acquisition Systems}
				\vspace{-2.3em}
				\begin{figure}
					\centering
					\includedot[width=0.7\textwidth]{data/la}	
				\end{figure}
				\vspace{-2.7em}
				\begin{itemize}
					\item Server-side transfer.
					\item Best triggered at acquisition conclusion, e.g. via \colorbox{tlg}{\texttt{repositorg\_push.sh}}:.
				\end{itemize}
				\inputminted[bgcolor=tlg]{bash}{scripts/repositorg_push.sh}
			\end{frame}
			\begin{frame}[fragile]{Microsoft Windows Acquisition Systems}
				\vspace{-2.3em}
				\begin{figure}
					\centering
					\includedot[width=0.7\textwidth]{data/ms}	
				\end{figure}
				\vspace{-2.7em}
				\begin{itemize}
					\item \textcolor{lg}{Server-side (analogous to Unix-like) if e.g. GitBash is available.}
					\item Client-side transfer with Samba.
					\item Best triggered by cron at regular down-times (add job via \colorbox{tlg}{\texttt{crontab -e}}):
				\end{itemize}
				\inputminted[bgcolor=tlg]{bash}{code/cron_samba}
			\end{frame}
	\section{Processing}
		\subsection{Minimal examples for minimal work.}
			\begin{frame}[fragile]{Image Processing}
				\inputminted[bgcolor=tlg,firstline=8,lastline=10]{bash}{scripts/EXAMPLE-UUID.sh}
				\begin{itemize}
					\item Input: Microscope \colorbox{tlg}{\texttt{.TIF}} raw data.
					\item Outputs: formatted (cropped), auto-leveled, and unstacked \colorbox{tlg}{\texttt{.tif}}.
					\item Using ImageMagick backend.
					\item Parallelized.
				\end{itemize}
			\end{frame}
			\begin{frame}[fragile]{Video Processing}
				\begin{minted}[fontsize=\footnotesize, bgcolor=tlg]{bash}
repositorg vidproc -p "-crf 16 -c:a copy -filter:v 'crop=1080:1080:420:0'" DSC1{80..84}.MOV
				\end{minted}
				\begin{itemize}
					\item Input: 3:4 recording with background in \colorbox{tlg}{\texttt{.MOV}} format.
					\item Output: 1:1 sample recording in \colorbox{tlg}{\texttt{.mkv}} format.
					\item Using FFmpeg backend.
					\item Parallelized.
				\end{itemize}
			\end{frame}
			\begin{frame}{But why Deterministic?}
				\vspace{-0.5em}
				\centering Checksum-based Continuous Data Collection Support:\\
				\vspace{1.5em}
				\centerline{
				\begin{minipage}{0.45\textwidth}
					Input directory contents:\\
					\vspace{-.7em}\\
					\texttt{\textcolor{mg}{
						5700 A1 5x w4transmission new.TIF
						5700 A2 5x w4transmission new.TIF
						5700 A3 5x w4transmission new.TIF
						5700 A4 5x w4transmission new.TIF
						5700 A5 5x w4transmission new.TIF
						5700 A6 5x w4transmission new.TIF
						\textcolor{myred}{5700 B1 5x w4transmission new.TIF}
						\textcolor{myyellow}{5700 B2 5x w4transmission new.TIF}
						\textcolor{myyellow}{5700 B3 5x w4transmission new.TIF}
					}}
				\end{minipage}%
				\begin{minipage}{0.1\textwidth}
				\centering
				$\longrightarrow$
				\end{minipage}%
				\begin{minipage}{0.33\textwidth}
					Output directory contents:\\
					\vspace{-.7em}\\
					\texttt{\textcolor{mg}{
						zeiss200m\_f2021.tif	
						zeiss200m\_f2022.tif	
						zeiss200m\_f2023.tif	
						\textcolor{myred}{zeiss200m\_f2024.tif}
						\textcolor{myyellow}{zeiss200m\_f2025.tif}	
						\textcolor{myyellow}{zeiss200m\_f2026.tif}
					}}
					\vspace{3.85em}
				\end{minipage}}
			\end{frame}
	\section{Workflow Example}
		\subsection{The Full Story.}
			\begin{frame}{Integrating Aforementioned Concepts and Functionalities}
				\vspace{-2em}
				\begin{figure}
					\includedot[width=1.03\textwidth]{data/workflow}	
				\end{figure}
				\vspace{-1.5em}
				\begin{itemize}
					\item Looks more complex than it is.
					\item Can be easily and robustly automated.
					\item Only 4 command lines ?!?!
				\end{itemize}
			\end{frame}
			\begin{frame}{Poorly named Microscopy Data From Removable Media}
				\begin{itemize}
					\item One cron entry \colorbox{tlg}{\texttt{crontab -e}} and reboot (only needs be done once):
					\inputminted[bgcolor=tlg,firstline=1,lastline=6]{bash}{scripts/crontab}
				\item One workflow file \colorbox{tlg}{\texttt{/home/myuser/.repositorg/sources/EXAMPLE-UUID.sh}}:(split)
					\inputminted[bgcolor=tlg,firstline=1,lastline=6]{bash}{scripts/EXAMPLE-UUID.sh}
				\end{itemize}
			\end{frame}
			\begin{frame}
				\inputminted[bgcolor=tlg,firstline=7]{bash}{scripts/EXAMPLE-UUID.sh}
			\end{frame}
	\section{References}
		\scriptsize
		\bibliographystyle{unsrt}
		\bibliography{./bib}

\end{document}
