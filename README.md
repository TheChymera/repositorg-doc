# Repositorg

Communications materials and documentation for the [Repositorg package](https://github.com/TheChymera/repositorg/).
Compiled binary documents (e.g. `*.pdf`) are also tracked for easer viewing; these should however only be committed for major revisions to avoid git cluttering.
It is therefore advisable to never do `git commit -a`, and to only commit binary documents explicitly.

## Compilation Instructions

### Presentaion

```sh
pdflatex -shell-escape pres.tex && bibtex pres && pdflatex -shell-escape pres.tex && pdflatex -shell-escape pres.tex
```
