\documentclass{beamer}
\usepackage[orientation=portrait,size=a0]{beamerposter}
\mode<presentation>{\usetheme{ZH_AIC}}
\usepackage[utf8]{inputenc}
\usepackage{multicol}
\usepackage{minted}
\usepackage{graphviz}
\usepackage{siunitx} %pretty measurement unit rendering
\usepackage{hyperref} %enable hyperlink for urls
\usepackage{ragged2e}
\usepackage{textcomp}
\usepackage[noabbrev]{cleveref}
\usepackage[font=scriptsize,justification=justified]{caption}

\sisetup{per-mode=symbol}

\definecolor{dy}{RGB}{202,202,0}
\definecolor{mg}{gray}{0.30}
\definecolor{lg}{gray}{0.60}
\definecolor{vlg}{gray}{0.75}
\definecolor{tlg}{gray}{0.92}
\definecolor{myred}{HTML}{EF0533}
\definecolor{mybrown}{HTML}{8B4513}

\definecolor{mypurple}{HTML}{8600E9}
\definecolor{mymagenta}{HTML}{D600D6}
\definecolor{mycyan}{HTML}{00A9A9}
\definecolor{mygreen}{HTML}{00Df1F}
\definecolor{myblue}{HTML}{0004F7}
\definecolor{myyellow}{HTML}{CAA200}
\definecolor{myred}{HTML}{E41F00}

\usepackage{array,booktabs,tabularx}
\newcolumntype{Z}{>{\centering\arraybackslash}X} % centered tabularx columns

\title{Repositorg: \huge FOSS \Huge Pipelines for Transfering, Repositing,\\ Renaming, and Standardizing Large Data Collections}
\author{Horea-Ioan Ioanas$^{1}$, Markus Rudin$^{1}$}
\institute[ETH]{$^{1}$Institute for Biomedical Engineering, ETH and University of Zurich}
\date{\today}

\newlength{\columnheight}
\setlength{\columnheight}{0.881\textheight}

\begin{document}
\begin{frame}
\vspace{1em}
\begin{columns}
	\begin{column}{.43\textwidth}
		\begin{beamercolorbox}[center]{postercolumn}
			\begin{minipage}{.98\textwidth}  % tweaks the width, makes a new \textwidth
				\parbox[t][.17\columnheight]{\textwidth}{ % must be some better way to set the the height, width and textwidth simultaneously
					\begin{myblock}{Background}
						Many data modalities, and optical imaging in particular, rely on large and heterogeneous collections of relatively small data files produced by proprietary programs on restrictively managed systems.
						The advent of big data, however, mandates data standardization and access to data on transparent and reproducibly managed data analysis systems.
						Efforts to manually enforce standardization, as well as manual repositing, fragment the imaging analysis workflow, increase the probability of data loss and corruption, and cannot guarantee perfect standard compliance.
						\vspace{.7em}

						Repositing files and standardizing formats and naming via simple in-house scripts entails significant effort duplication across groups, encourages standards divergence, and creates unsustainable workflow dependencies.
						Here we present a pipeline package designed to accelerate and automate
						(a) data transfer between a potentially proprietary acquisition and an analysis system,
						(b) basic preprocessing in order to enforce data format standards,
						(c) file renaming to continuous namespaces with duplicate detection.
					\end{myblock}\vfill
		}\end{minipage}\end{beamercolorbox}
	\end{column}
	\begin{column}{.57\textwidth}
		\begin{beamercolorbox}[center]{postercolumn}
			\begin{minipage}{.98\textwidth} % tweaks the width, makes a new \textwidth
				\parbox[t][.17\columnheight]{\textwidth}{ % must be some better way to set the the height, width and textwidth simultaneously
					\begin{myblock}{Workflow Directory Hierarchy}
						\begin{minipage}{.62\textwidth}
							\vspace{1.2em}
							\begin{figure}
								\includegraphics[width=\textwidth]{img/tree.png}
								\caption{Workflow directory tree (example shipped with package, \colorbox{tlg}{\texttt{repositorg\_push.sh}} is an acquisition-side example file).}
								\label{fig:tree}
							\end{figure}
						\end{minipage}
						\begin{minipage}{.02\textwidth}
						\hspace*{.01em}	
						\end{minipage}
						\begin{minipage}{.32\textwidth}
							Repositorg workflow files are stored in an unobtrusive eponymous hidden directory in the user's home path.
							In addition to workflows, this directory contains the \colorbox{tlg}{\texttt{UUID\_trigger.sh}} script, which, while executed, provides Universally Unique Identifier (UUID) triggerig capabilities.
							The workflows themselves are located under \colorbox{tlg}{\texttt{.repositorg/sources}}, with UUID-based workflows located under \colorbox{tlg}{\texttt{.repositorg/sources/UUIDs}} for unambiguous execution.
						\end{minipage}
					\end{myblock}\vfill
		}\end{minipage}\end{beamercolorbox}
	\end{column}
\end{columns}
\begin{beamercolorbox}[center]{postercolumn}
	\begin{minipage}{.98\textwidth}  % tweaks the width, makes a new \textwidth
		\parbox[t][.215\columnheight]{\textwidth}{ % must be some better way to set the the height, width and textwidth simultaneously
			\begin{myblock}{Workflow Overview}
				\centering
				\vspace{-3.5em}
				\begin{figure}[h!]
					\hspace*{-4em}
                                        \includedot[width=1.08\textwidth]{data/workflow}
                		\end{figure}
				\vspace{-3em}
			\end{myblock}\vfill
}\end{minipage}\end{beamercolorbox}
\begin{columns}
	\begin{column}{.57\textwidth}
		\begin{beamercolorbox}[center]{postercolumn}
			\begin{minipage}{.98\textwidth}  % tweaks the width, makes a new \textwidth
				\parbox[t][.63\columnheight]{\textwidth}{ % must be some better way to set the the height, width and textwidth simultaneously
					\begin{myblock}{Removable Media Source}
						\begin{minipage}{.565\textwidth}
							\vspace{1em}
							Automatic repositing from removable media can be acchieved via the \colorbox{tlg}{\texttt{UUID\_trigger.sh}} script.
							The script can listen for and react to mount events, and is best started at boot time.
							To automatically start upon the next boot event, run \colorbox{tlg}{\texttt{crontab -e}}, and add the following line:
							\inputminted[bgcolor=tlg]{bash}{code/cron_uuid}
						\end{minipage}
						\begin{minipage}{.03\textwidth}
						\hspace*{.01em}	
						\end{minipage}
						\begin{minipage}{.405\textwidth}
							\vspace{-2em}
							\begin{figure}
								\hspace*{-3em}	
								\includedot[width=1.2\textwidth]{data/rm}
								\vspace*{-2em}
								\caption{File-level overview of removable media source auto-transfer.}
							\end{figure}
						\end{minipage}
						The UUID trigger script will monitor mount events, determine the UUIDs of mounted devices, and immediately execute eponymous workflows, if any exist under \colorbox{tlg}{\texttt{.repositorg/sources/UUIDs/}} (as seen in \cref{fig:tree}).
							\vspace*{-3em}
							\begin{figure}
								\hspace*{-3em}	
								\includedot[width=1\textwidth]{data/rms}
								\vspace*{-2.5em}
								\caption{Event and script execution overview of auto-transfer from removable media.}
							\end{figure}
						\vspace{-0.5em}
					\end{myblock}\vfill
					\begin{myblock}{Unix-like Acquisition System Source}
						\begin{minipage}{.53\textwidth}
							\vspace{1em}
							For automatic repositing from Unix-like acquisition systems --- or any acquisition systems with Secure Shell (SSH) and programmatic data transfer capabilities --- the workflow can be triggered upon acquisition conclusion.
							Acquisition conclusion can be signalled by running a push script on the acquisition machine.
							Such an example script is included in the package (see \cref{fig:tree}), and may look like this:
						\end{minipage}
						\begin{minipage}{.03\textwidth}
						\hspace*{.01em}	
						\end{minipage}
						\begin{minipage}{.44\textwidth}
							\vspace{-1em}
							\begin{figure}
								\hspace*{-4em}	
								\includedot[width=1.2\textwidth]{data/la}
								\vspace*{-2em}
								\caption{File-level overview of Unix-like acquisition system source auto-transfer.}
							\end{figure}
						\end{minipage}
							\inputminted[bgcolor=tlg]{bash}{scripts/repositorg_push.sh}
							
							\vspace*{-.5em}
						When \colorbox{tlg}{\texttt{repositorg\_push.sh}} is executed remotely, the data will be transfered to the data analysis machine, and the remainging workflow will be triggered via a post-hook. 
							\vspace*{-2em}
							\begin{figure}
								\hspace*{-3em}	
								\includedot[width=1\textwidth]{data/las}
								\vspace*{-2.5em}
								\caption{Event and script execution overview of auto-transfer from Unix-like acquisition system sources.}
							\end{figure}
						\vspace{-0.5em}
					\end{myblock}\vfill
					\begin{myblock}{The \texttt{repositorg fetch} Function}
						This function provides copy functionality, which is aware of and parameterizes the identifier for Repositorg primary copy storage (\colorbox{tlg}{\texttt{/var/tmp/repositorg/<SOURCE-ID>}}).
						Prospectively, it is also intended to provide seamless \colorbox{tlg}{\texttt{smbclient}} integration, in order to permit more uniform workflow design.
					\end{myblock}\vfill
		}\end{minipage}\end{beamercolorbox}
	\end{column}
	\begin{column}{.43\textwidth}
		\begin{beamercolorbox}[center]{postercolumn}
			\begin{minipage}{.98\textwidth} % tweaks the width, makes a new \textwidth
				\parbox[t][.63\columnheight]{\textwidth}{ % must be some better way to set the the height, width and textwidth simultaneously
					\begin{myblock}{Microsoft Windows Acquisition System Source}
						\begin{minipage}{.44\textwidth}
							\vspace{.5em}
							Given no shell capabilities on the acquisition machine, but given network access to a Samba share (often the case for Windows sources), automatic repositing can be triggered by configuring a cron job. 
						\end{minipage}
						\begin{minipage}{.03\textwidth}
						\hspace*{.01em}	
						\end{minipage}
						\begin{minipage}{.53\textwidth}
							\vspace{-1.5em}
							\begin{figure}
								\hspace*{-6.5em}	
								\includedot[width=1.15\textwidth]{data/ms}
								\hspace*{-5em}	
								\vspace*{-2em}
								\caption{File-level overview of Windows source periodic transfer.}
							\end{figure}
						\end{minipage}

						\vspace*{0.5em}	
						Such a job can be registered to be executed e.g. weekly by running \colorbox{tlg}{\texttt{crontab -e}}, and adding:

							\inputminted[bgcolor=tlg]{bash}{code/cron_samba}
						
						\vspace*{-1em}	
						The Samba workflow script (with \colorbox{tlg}{\texttt{smbclient}} fetching) is executed weekly (time and day given in \colorbox{tlg}{\texttt{/etc/crontab}}).
							\vspace*{-3.6em}
							\begin{figure}
								\hspace*{-3em}	
								\includedot[width=1\textwidth]{data/mss}
								\vspace*{-3.5em}
								\caption{Event and script execution overview of periodic transfer from Windows sources.}
							\end{figure}
						\vspace*{-1em}
					\end{myblock}\vfill
					\begin{myblock}{The \texttt{repositorg *proc} Functions}
						The \colorbox{tlg}{\texttt{imgproc}}, and \colorbox{tlg}{\texttt{audioproc}} and \colorbox{tlg}{\texttt{vidproc}} subfunctions are thin, Repositorg-path-aware and parallelization-enabling wrappers for ImageMagick's \colorbox{tlg}{\texttt{convert}} and FFmpeg's \colorbox{tlg}{\texttt{ffmpeg}}, respectively.
						\vspace{-2.7em}
						\begin{figure}
							\hspace*{-3em}	
							\includedot[width=1\textwidth]{data/procs}
							\vspace*{-2.5em}
							\caption{Overview of \colorbox{tlg}{\texttt{imgproc}} internal parameter passing and multiprocess management, representative of all \colorbox{tlg}{\texttt{repositorg *proc}} subfunctions.}
						\end{figure}
						\vspace*{-1em}
					\end{myblock}\vfill
					\begin{myblock}{The \texttt{repositorg reposit} Function}
						This function redefines file names to enforce naming and directory structure standards.
						For maximum parsing power, field values are sourced via Regular Expressions (regex).
						For clarity, the output file name and directory structure template is specified as one formattable string.
						\vspace*{.4em}
						\begin{itemize}
							\item Highlighted values can be captured:\\
								\centerline{\texttt{\textcolor{mg}{\textcolor{myred}{5700}\_\textcolor{mygreen}{A3}\_\textcolor{myblue}{5}x\_w4\textcolor{myyellow}{transmission} new.\textcolor{mypurple}{TIF}}}}
							\item And used to create a more explicit naming pattern and hierarchy:\\
								\centerline{\texttt{\textcolor{mg}{sub-\textcolor{myred}{5700}/sub-\textcolor{myred}{5700}\_slice-\textcolor{mygreen}{a3}\_zoom-\textcolor{myblue}{5}\_\textcolor{myyellow}{transmission}.\textcolor{mypurple}{tif}}}}
						\end{itemize}
						
						\vspace*{.5em}
						Counter fields (\colorbox{tlg}{\texttt{\{LETTERS\}}} or \colorbox{tlg}{\texttt{\{DIGITS\}}} --- useful for sequential collections) are also available in the formattable (Python) string.
						If the destination directory is populated, the function will check for the latest common file (determined via checksum), and only reposit subsequent files:
						
						\vspace*{1em}
						\centerline{
                                \begin{minipage}{0.45\textwidth}
                                        Input directory contents:\\
                                        \vspace{-.7em}\\
                                        \texttt{\textcolor{mg}{
                                                5700-A5-5xnew.TIF
                                                5700-A6-5xnew.TIF
                                                \textcolor{myred}{5700-B1-5xnew.TIF}
                                                \textcolor{myyellow}{5700-B2-5xnew.TIF}
                                                \textcolor{myyellow}{5700-B3-5xnew.TIF}
                                        }}
                                \end{minipage}%
                                \begin{minipage}{0.1\textwidth}
                                \centering
                                $\longrightarrow$
                                \end{minipage}%
                                \begin{minipage}{0.33\textwidth}
                                        Output directory contents:\\
                                        \vspace{-.7em}\\
                                        \texttt{\textcolor{mg}{
                                                zeiss200m\_f2023.tif
                                                \textcolor{myred}{zeiss200m\_f2024.tif}
                                                \textcolor{myyellow}{zeiss200m\_f2025.tif}
                                                \textcolor{myyellow}{zeiss200m\_f2026.tif}
                                        }}
                                        \vspace{1.2em}
                                \end{minipage}}

					\end{myblock}\vfill
		}\end{minipage}\end{beamercolorbox}
	\end{column}
\end{columns}
\end{frame}
\end{document}
